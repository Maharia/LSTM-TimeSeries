from pandas import read_csv, datetime, DataFrame, concat, Series
from sklearn.preprocessing import MinMaxScaler
import numpy

def parser(x):
    return datetime.strptime('190'+x, '%Y-%m')


class ProcessData:

    scaler = None

    def prepare_input_data(self,data,lag=1):
        """
            This method converts time series data to input output.
            If we have to predict the value at next timestamp i.e t+1, then
            the value at t will be input and value at t+1 will be output

            so the algo is, we shift the data by one step. lets call this one
            dataframe i.e df1, then we merge this with the original dataframe
        """
        series = DataFrame(data)
        dataframes = []
        for i in range(1, lag+1):
            dataframes.append(series.shift(i))
        dataframes.append(series)
        series = concat(dataframes, axis=1)
        series.fillna(0, inplace=True)
        return series


    def invert_differencing(self,predicted_value, idx):
        """
            This method will inverse the difference that we do to make the data
            stationary
        """
        print "data ", self.history[idx]
        return predicted_value + self.history[idx]


    def perform_differencing(self,dataset,interval=1):
        """
            This will make the timeseries stationary. Prior to this there will be 
            structure in the data that will be dependent on time. It will either
            increase with time or decrease with time. Stationary data is easier to
            model and will very likely result in more skillful forecasts.

            We make the data stationary by using differencing the data.
        """
        self.history = dataset
        diff = list()
        for i in range(interval, len(dataset)): 
            value = dataset[i] - dataset[i-interval]
            diff.append(value)
        return Series(diff)

    def perform_scaling(self,data):
        """
            Like other neural network algorithms, LSTM expects the input data within
            the range of activation functions. The activation function for LSTM is
            hyperbolic tangent which ranges within -1 to 1. So we need to scale out
            training and test data in -1 to 1
        """
        if(not self.scaler):
            self.scaler = MinMaxScaler(feature_range=(-1, 1))
        return self.scaler.fit_transform(data)

    def invert_scaling(self,values):
        arr = numpy.array(values)
	arr = arr.reshape(1, len(arr))
	inverted = self.scaler.inverse_transform(arr)
	return inverted[0,-1]



    def init(self,filename, lag,testsize):
        # data = read_csv(filename, header=0, parse_dates=[0],index_col=0,squeeze=True,date_parser=parser)
        data = []
        with open(filename) as f:
            lines = f.readlines()
            for l in lines:
                data.append(float(l.split(',')[1]))
        print(data)
        # differenced_series = self.perform_differencing(data.values, 1)
        differenced_series = self.perform_differencing(data, 1)
        differenced_values = differenced_series.values
        differenced_values = differenced_values.reshape(len(differenced_values), 1)
        scaled_values = self.perform_scaling(differenced_values)
        scaled_values = scaled_values.reshape(len(scaled_values), 1)
        supervised_data = self.prepare_input_data(scaled_values, lag)
        supervised_data_values = supervised_data.values
        train, test = supervised_data_values[0:-testsize], supervised_data_values[-testsize:]
        return data, train, test
        # return data.values,train_scaled,test_scaled


if __name__ == "__main__":
    process_data = ProcessData()
    process_data.init('data.csv', 4)


