from keras.models import Sequential
from keras.layers.core import Dense,Dropout
from keras.layers import LSTM
from sklearn.metrics import mean_squared_error
from math import sqrt
import numpy as np


class LSTMModel:
    
    model = None
    batch_size = None
    neurons = None
    epochs = None

    def __init__(self, batch_size, neurons, epochs):
        import numpy as np
        np.random.seed(42)
        import tensorflow as tf
        tf.set_random_seed(42)
        self.batch_size = batch_size
        self.neurons = neurons
        self.epochs = epochs

    def build_model(self, x_train):
        model = Sequential()
        model.add(LSTM(self.neurons,batch_input_shape=(1, x_train.shape[1], x_train.shape[2]), stateful=True))
        model.add(Dense(1))
        model.compile(loss="mean_squared_error", optimizer="adam")
        self.model = model


    def fit(self,train, lag):
        x_train = train[:, 0:lag]
        y_train = train[:, lag:]
        x_train = np.reshape(x_train, (x_train.shape[0],1,x_train.shape[1]))
        self.build_model(x_train)
        for i in range(self.epochs):
            self.model.fit(x_train, y_train, batch_size=1,epochs=1,shuffle=False,validation_split=0.05)
            self.model.reset_states()
    
    def predict(self,data):
        data = data.reshape(1, 1, len(data))
        predicted = self.model.predict(data, batch_size=1)
        return predicted[0, 0]

