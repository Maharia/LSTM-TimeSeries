from process_data import ProcessData
from lstm import LSTMModel
import sys
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error
from math import sqrt
import numpy as np

def plot_results(predicted_data, true_data):
    print "Hello World"
    fig = plt.figure(facecolor='white')
    ax = fig.add_subplot(111)
    ax.plot(true_data, label='True Data')
    plt.plot(predicted_data, label='Prediction')
    plt.legend()
    fig.savefig('/Users/Maharia/Projects/pr/time-series-analysis-univariate/to.png')  # save the figure to file
    plt.close(fig)

    percent_error = []
    for i in range(len(predicted_data)):
        percent_error.append(abs(predicted_data[i] - true_data[i]) / true_data[i])
    # rmse = sqrt(mean_squared_error(series[-12:], predictions))
    rmse = np.mean(percent_error)
    print('Test RMSE: %.3f' % rmse)
    # plt.show()


def denormalise_predictions(data_processor,predictions,test_scaled,series, train_scaled):
    new_preds = []
    previous_pred = None
    for i in range(len(test_scaled)):
        X, y = test_scaled[i, 0:-1], test_scaled[i, -1]
        prediction = data_processor.invert_scaling(X, predictions[i])
        prediction = data_processor.invert_differencing(prediction, len(test_scaled) - i)
        new_preds.append(prediction)
        actual = series[len(train_scaled) + i + 1]
        print('Day=%d, Predicted=%f, Actual=%f' % (i+1, prediction, actual))
    return new_preds

def walk_forward_validation(lstm_model, test_scaled,lag):
    predictions = []
    for i in range(len(test_scaled)):
        X, y = test_scaled[i, 0:lag], test_scaled[i, lag:]
        print X
        prediction = lstm_model.predict(X)
        predictions.append(prediction)
    return predictions


def inverse_transformations(data_processor,series, predictions, test_size, train_size):
    inverted_values = []
    for i in range(len(predictions)):
        scale_inverse = data_processor.invert_scaling([predictions[i]])
        idx = len(series) - test_size + i - 1
        diff_inverse = data_processor.invert_differencing(scale_inverse, idx)
        expected = series[train_size + i + 1]
        inverted_values.append(diff_inverse)
        print('Day=%d, Predicted=%f, Actual=%f' % (i+1,diff_inverse, expected))

    return inverted_values

def main(filename, lag, batch_size=1, neurons=4, epochs=1,test_size=7):
    data_processor = ProcessData()
    lstm_model =LSTMModel(batch_size, neurons, epochs)
    series,train_scaled, test_scaled = data_processor.init(filename, lag,test_size)
    lstm_model.fit(train_scaled, lag)
    for i in range(len(train_scaled)):
        X, y = train_scaled[i, 0:lag], train_scaled[i, lag]
        lstm_model.predict(X)
    new_preds = walk_forward_validation(lstm_model,test_scaled,lag)
    new_preds = inverse_transformations(data_processor,series,new_preds, test_size, len(train_scaled))
    plot_results(new_preds, series[-test_size:])


if __name__=="__main__":
    filename = sys.argv[1]
    lag = sys.argv[2]
    epoch = sys.argv[3]
    main(filename,int(lag), epochs=int(epoch))



